import logo from './logo.svg';
import './App.css';
//import {BrowserRouter as Router, router, Switch, Link} from 'react-router-dom';
import Homepage from './Homepage'

import{
  BrowserRouter as Router,
  Route,
  Switch,
}from 'react-router-dom';

function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path = "/">
            <Homepage/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
